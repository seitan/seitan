var server = require('http').createServer()
var express = require('express')
var app = express()
var WebSocketServer = require('ws').Server
var wss = new WebSocketServer({ server: server, port: 8000 })

app.use(express.static('static')) // client code goes in static directory

var clients = []
wss.on('connection', function (ws) {
  ws.on('message', function (inputStr) {
    var wsmsg
    try {
      wsmsg = JSON.parse(inputStr)
      if (/^(ad|object)$/.test(wsmsg.type) === false) return
      if (/^[A-z0-9]{2,25}$/.test(wsmsg.handle) === false) return
      if (/^[A-z0-9]{2,64}$/.test(wsmsg.room) === false) return
      if (typeof(wsmsg.connectionMode) !== 'undefined' && /^[A-z0-9]{2,64}$/.test(wsmsg.connectionMode) === false) return
    } catch (e) {
      console.log('Invalid websocket message')
      console.log(e)
      return
    }
    console.log(inputStr)

    if (wsmsg.type == 'ad') {
      var alreadyIn = false
      for (var i = clients.length - 1; i >= 0; i--) {
        if (clients[i].ws === ws) {
          alreadyIn = true
        } else if (clients[i].handle == wsmsg.handle) {
          console.log('Dropping old mapping to ' + wsmsg.handle)
          clients.splice(i, 1)
        }
      }
      if (alreadyIn === false) {
        clients.push({ handle: wsmsg.handle, room: wsmsg.room, ws: ws, ts: Date.now() })
      }

      for (var j = clients.length - 1; j >= 0; j--) {
        if (clients[j].handle != wsmsg.handle && clients[j].room === wsmsg.room) {
          clients[j].ws.send(JSON.stringify({ type: 'newmember', handle: wsmsg.handle, connectionMode: wsmsg.connectionMode }))
        }
      }
    } else if (wsmsg.type === 'object') {
      for (var k = clients.length - 1; k >= 0; k--) {
        if (clients[k].room === wsmsg.room) {
          if (wsmsg.to === clients[k].handle || wsmsg.to === 'everyone') {
            clients[k].ws.send(inputStr)
          }
        }
      }
    }
  })
  ws.on('close', function (data) {
    console.log('Received close on a websocket')
    for (var l = clients.length - 1; l >= 0; l--) {
      if (clients[l].ws.readyState === 3) {
        clients.splice(l, 1)
      }
    }
  })
})

server.on('request', app)
server.listen(37080, '0.0.0.0', function () { console.log('Listening on ' + server.address().port) })
