# Seitan - Peer to peer collaboration environment #

Seitan is a tool for hackers that provides a P2P environment where sensitive chat, pastes and files can be sent and received over secure channel, without any data-at-rest-on-a-cloud-server interference.

You can chat, exchange pastes and files, copypaste screenshots to chat and work together on a same document. Additionally Seitan now supports voice chat as well as sharing a screen presentation.

Seitan works by forming WebRTC datachannels between member browsers and all created content is sent over these peer to peer channels and stored within the limits of browser javascript variables. No cookies, no localstorage, no cloud storage, one page refresh and all the data is gone. If you feel like saving the session, you can do so by performing a backup. A backed up session can be restored including uploaded files.

Seitan does come with some caveats. You cannot form any connections without a signaling server (signaling_server.js) that allows parties to exchange communication offers and answers. Even with the help of signaling server you might have trouble forming connections from behind different firewalls, for these type of connections you might need a TURN server, this repo has necessary configs and instructions for [Coturn TURN server](https://github.com/coturn/coturn) but feel free to use your own.

## Install to debian (sample instructions)

n00bs can use these instructions to run and test their server, 1337haxx0rs can modify everything to better suit their needs.

    sudo apt update
    sudo apt install git python3-pip docker.io openssl
    sudo python3 -m pip install docker-compose
    git clone https://gitlab.com/seitan/seitan.git
    cd seitan
    
    openssl req  -nodes -new -x509 -keyout YOU-NEED-TO-CREATE-THIS.key -out YOU-NEED-TO-CREATE-THIS.crt -subj "/C=US/ST=MyState/L=MyCity/O=MyO/CN=myserver"
    
    sudo docker-compose up
    
After these steps, open a browser and connect to (replace SERVER_ADDRESS with your servers IP address or hostname):

    https://SERVER_ADDRESS/seitan
    
You will need to configure the used STUN/TURN information, the page will offer you link to perform future logins quicker. Here is a sample configuration, replace SERVER_ADDRESS with your servers IP address or hostname:

    SIGNALING: wss://SERVER_ADDRESS/sss
    STUN: stun:SERVER_ADDRESS
    TURN: turn:SERVER_ADDRESS
    TURN username: coturnanonymous
    TURN password: coturnpassword
    
If you plan on running your STUN/TURN server in AWS (or similar environment), you might want to set the external-ip setting. I encourage you to change other Coturn settings as well.
    
## Architecture ##

Seitan is based on the idea of having minimal number of libraries and files. This should make auditing the code easier. This repository has 3 main elements:

### Seitan p2p client ###

This is a single html file that contains javascript code that allows you to connect to signaling and TURN servers as well as other parties. It also contains all the actual features such as chat, pastes, file-exchange, collab, voice and so on.

### Seitan Signaling Server ###

This is a nodejs server that provides websocket-channel for exchanging WebRTC connection creation messages. It also provides a way to have multiple rooms to have chats with.

### Coturn Server [OPTIONAL] ###

Sample configuration for Coturn STUN/TURN server. Coturn project can be found [here](https://github.com/coturn/coturn)

## Security ##

### Firewall ###

All ports are configurable, the default ports are as follows:

* Signaling server requires port 443 to be open.
* STUN/TURN server is by default using port 3478 - also UDP
* TURN server can be widely configured, but it is recommended to have at least around 1000 available UDP ports for connections.

### Additional security info ###

By default, all Webrtc communications are encrypted. By current limitations of javascript subtle crypto library, you cannot even calculate sha256 hash if you are not receiving instruction to do so over HTTPS -connection.

Having encrypted TURN server traffic is not a must, as the traffic passing through it is encrypted already.

## Thats a bacon emoji, not Seitan ##

I know.