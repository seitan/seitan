FROM node:latest

RUN npm install ws express

#COPY signaling_server.js signaling_server.js

EXPOSE 8000

CMD ["node", "signaling_server.js"]
